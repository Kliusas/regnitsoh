@extends('layout.layout')

@section('content')
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <h1>Recursive Printing</h1>
            <hr>
            <ul>
                @foreach ($recursiveCategories as $recursiveCategory)
                    @include('partials.recursive', $recursiveCategory)
                @endforeach
            </ul>
        </div>
    </div>
@endsection