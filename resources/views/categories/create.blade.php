@extends('layout.layout')

@section('content')
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <h1>Category Creation</h1>
            <hr>
            <form action="/categories" method="post">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="name">Category Name:</label>
                    <input type="text" class="form-control" id="name"  name="name" maxlength="20" required>
                </div>

                <div class="form-group">
                    <label for="parent">Select Parent Category:</label>
                    <select class="form-control" id="parent"  name="parent">
                        <option value="" selected="selected">Mark As Parent</option>
                        @foreach ($categories as $category)
                            <option value ="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </form>
        </div>
    </div>
@endsection