<li>{{ $recursiveCategory['name'] }}</li>
@if (isset($recursiveCategory['children']) && count($recursiveCategory['children']) > 0)
    <ul>
        @foreach($recursiveCategory['children'] as $recursiveCategory)
            @include('partials.recursive', $recursiveCategory)
        @endforeach
    </ul>
@endif