
## TASK
 The exercise is printing a categories tree to the page (with unlimited
 hierarchy) in two different ways:

- Recursive
- Iterative

## Important to note
- Add a form for adding new categories
- Should be written in OOP
- Use design patterns when needed
- Make sure the code is simple and clean
- Separate concerns
- Testable code
- Send finished program Github repository link to
giedrius@hostinger.com



## System Requirements

Standard Laravel requirements: https://laravel.com/docs/5.5/installation

## Quick Start

- Run composer install to install external packages
- Copy .env.example to .env and fill it with Your environment configurations
- Generate application key by running php artisan key:generate
- Migrate dabatase: php artisan migrate
- Run built in server: php artisan serve (or configure any other web server - document root: /public)
- /categories/create - you can add category with foreign key to other category
- /categories/recursive - recursive printing unlimited depth categories