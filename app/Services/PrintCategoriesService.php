<?php
declare(strict_types = 1);

namespace App\Services;

/**
 * Class PrintCategoriesService
 * @package App\Services
 */
class PrintCategoriesService
{
    /**
     * @param array $categories
     * @param null $parent
     * @return array
     */
    public function makeArrayTree(array &$categories, $parent = NULL) {

        $branch = array();

        foreach ($categories as $category)
        {
            if ($category['parent_id'] == $parent)
            {
                $children = $this->makeArrayTree($categories, $category['id']);

                if ($children)
                {
                    $category['children'] = $children;
                }

                $branch[$category['id']] = $category;
            }
        }

        return $branch;
    }

}