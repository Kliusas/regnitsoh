<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Repositories\CategoriesRepository;
use App\Services\PrintCategoriesService;
use Illuminate\View\View;

class CategoriesController extends Controller
{

    /**
     * @var \Illuminate\Database\Eloquent\Collection
     */
    private $categories;

    /**
     * @var CategoriesRepository
     */
    private $categoriesRepository;

    /**
     * @var PrintCategoriesService
     */
    private $printCategoriesService;

    /**
     * CategoriesController constructor.
     * @param CategoriesRepository $categoriesRepository
     * @param PrintCategoriesService $printCategoriesService
     */
    public function __construct(CategoriesRepository $categoriesRepository, PrintCategoriesService $printCategoriesService)
    {

        $this->categoriesRepository = $categoriesRepository;

        $this->categories = $categoriesRepository->all();

        $this->printCategoriesService = $printCategoriesService;
    }

    /**
     * @return View
     */
    public function create(): View
    {

        return view('categories.create', ['categories' => $this->categories]);
    }

    /**
     * @param CategoryRequest $request
     * @return mixed
     */
    public function store(CategoryRequest $request)
    {
        $data = [
            'name' => $request->getName(),
            'parent_id' => $request->getParentCategoryId()
        ];

        $this->categoriesRepository->create($data);

        return redirect()->back()->withSuccess('Category has been added');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function recursive(): View
    {
        $categoriesToArray = $this->categories->toArray();
        $recursiveCategories = $this->printCategoriesService
                                    ->makeArrayTree($categoriesToArray, NULL);
       
        return view('categories.recursive', ['recursiveCategories' => $recursiveCategories]);
    }
}
