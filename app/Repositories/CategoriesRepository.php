<?php
declare(strict_types = 1);

namespace App\Repositories;

use App\Models\Category;

/**
 * Class CategoriesRepository
 * @package App\Repositories
 */
class CategoriesRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return Category::class;
    }

}