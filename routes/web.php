<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('categories/create', 'CategoriesController@create')->name('categories.create');
Route::get('categories/iterative', 'CategoriesController@iterative')->name('categories.iterative');
Route::get('categories/recursive', 'CategoriesController@recursive')->name('categories.recursive');
Route::post('categories/', 'CategoriesController@store')->name('categories.store');